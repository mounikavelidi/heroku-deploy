pytest==4.4.1
pytest-ordering==0.6
docker==3.7.2
requests2==2.16.0
pyaml==18.11.0
bitbucket_pipes_toolkit==1.2.0
