# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.0.1

- patch: Internal maintenance: update pipes toolkit version.

## 1.0.0

- major: Changed variable WAIT to false by default to reduce the time of pipe execution

## 0.1.2

- patch: Minor documentation updates

## 0.1.1

- patch: Fixed WAIT parameter type conversion

## 0.1.0

- minor: Initial release
- patch: Fixed the build script

